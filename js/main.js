

var Aqua;
(function ($) {
    Aqua = {
        init : function () {
        },
        log : function () {
            console.log("qwdqwd");
        },
        categoriesClick : function () {
            $(".heading-no-line").on("click", function () {
                $("li").removeClass("li-clicked");
                $(this).addClass("li-clicked");
            })
        },
        slider : function ($elem, opts) {
            var option = {};
            if(!opts) {
                option = {
                    variableWidth: false,
                    dots: false,
                    infinite: true,
                    slidesToShow: 7,
                    slidesToScroll: 1,
                    infinite: true,
                    touchMove:false,
                    focusOnSelect: false,
                    arrows: true,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: false,
                                centerMode: true
                            }
                        },
                        {
                            breakpoint: 760,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: false,
                                centerMode: false
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: false,
                                centerMode: false
                            }
                        }
                    ]
                };
            }
            else {option = JSON.parse(opts);}
            console.log(option);
            $elem.parent().slick(option);
        },
        accordion: function ($elem) {
            $elem.accordion({collapsible: true});
        },

        sliderNotRepeat: function ($elem, opt) {
            var option = JSON.parse(opt);
            $elem.slick(option);
        }


    }
})(jQuery);

var angularApp = angular.module("angularAqua", ["ngRoute", "ngSanitize", "ngAnimate"]);

angularApp.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "app/components/pages/home.html"
        })
        .when("/home-basic", {
            templateUrl : "app/components/pages/home-basic.html"
        })
        .when("/home-fullwidth", {
            templateUrl : "app/components/pages/home-fullwidth.html"
        })
        .when("/home-fullheight", {
            templateUrl : "app/components/pages/home-fullheight.html"
        })
        .when("/home-onepage", {
            templateUrl : "app/components/pages/home-onepage.html"
        })
        .when("/home-shoping", {
            templateUrl : "app/components/pages/home-shoping.html"
        })
        .when("/home-update1", {
            templateUrl : "app/components/pages/home-update1.html"
        })
        .when("/home-update2", {
            templateUrl : "app/components/pages/home-update2.html"
        })
        .when("/about-us", {
            templateUrl : "app/components/pages/about-us.html"
        })
        .when("/error404", {
            templateUrl : "app/components/404.html"
        })
        .when("/underconstruction", {
            templateUrl : "app/components/pages/underconstruction.html"
        })
        .when("/service", {
            templateUrl : "app/components/pages/service.html"
        })
        .when("/shop", {
            templateUrl : "app/components/pages/shop.html"
        })
        .when("/shop-product", {
            templateUrl : "app/components/pages/shop-product.html"
        })
        .when("/shop-cart", {
            templateUrl : "app/components/pages/shop-cart.html"
        })
        .when("/shop-checkout", {
            templateUrl : "app/components/pages/shop-checkout.html"
        })
        .when("/blog-homepage", {
            templateUrl : "app/components/pages/blog-homepage.html"
        })
        .when("/blog-article", {
            templateUrl : "app/components/pages/blog-article.html"
        })
        .when("/blog-sidebar", {
            templateUrl : "app/components/pages/blog-sidebar.html"
        })
        .when("/reservation", {
            templateUrl : "app/components/pages/reservation.html"
        })
        .when("/contact", {
            templateUrl : "app/components/pages/contact.html"
        })
        .when("/#", {
            templateUrl : "app/components/pages/contact.html"
        });

});


angularApp.controller("templateController", function ($scope, $http, dataService, $location) {

    var jsonURL = $scope.json;

    if(jsonURL) {
        $http ({
            method: "GET",
            url: "data/" + $scope.json + ".json",
        }).then(function onSuccess(response) {
            $scope.datablock = response.data;
        });
    }
    eval($scope.callback);


});

angularApp.controller("partController", function ($scope) {
    $scope.shoppingTemplate = 'app/components/shopping/shop-beauty-template.html';
    $scope.printOut = function () {
        console.log($scope.shoppingTemplate);
    };
});

angularApp.directive("aquaPart", function () {
    return {
        link: function (scope, elem, attr) {
            (function ($) {
                $(".heading-no-line").on("click", function () {
                    $("li").removeClass("li-clicked");
                    $(this).addClass("li-clicked");
                })
            })(jQuery)
        },
        controller: "partController"
    }
});



angularApp.controller("onepageTestimonialController", function ($scope, dataService, $timeout) {
    $scope.i = 3;
    $scope.contentChanged = false;
    //$scope.data1 = dataService.getData();
    $scope.$watch (function (scope) {
        return scope.$parent.datablock;
    },
    function () {
        if (typeof $scope.$parent.datablock !== 'undefined') {
            $scope.jsonData = $scope.$parent.datablock;
            changeComment($scope.i);
        }
    }
    );


    var changeComment = function () {
        $scope.contentChanged = true;
        $timeout(function () {
            $scope.author = $scope.jsonData[$scope.i].name;
            $scope.comment = $scope.jsonData[$scope.i].comment;
        }, 500);


        $timeout(function () {
            $scope.contentChanged = false;
        }, 600);
    }


    $scope.nextComment = function () {



        if($scope.i < $scope.jsonData.length - 1) {
            $scope.i++;
        }
        else {
            $scope.i=0;
        }
        changeComment($scope.i);
    };

    $scope.previousComment = function () {
        if($scope.i == 0) {
            $scope.i = $scope.jsonData.length - 1;
        }
        else {
            $scope.i--;
        }
        changeComment($scope.i);
    }



    // $scope.$watch("author", function () {
    //     console.log(angular.element("#comment-author"));
    //     angular.element("#comment-author").addClass("changed");
    //     $timeout(function () {
    //         angular.element("#comment-author").removeClass("changed")
    //     }, 1000);
    // });

});


angularApp.directive("aquaTemplate", function () {
   return {
       templateUrl: function(elem, attr) {
           return "app/components/" + attr.template + ".html";
       },
       restrict: "E",
       scope: {
           json: "@",
           callback: "@",
           template: "@"
       },
        controller: "templateController"
   }
});

angularApp.directive("aquaSlider", function ($timeout) {
    return function (scope, elem, attr) {
            if(scope.$last) {
                 Aqua.slider(elem, attr.aquaSlider);
            }
        }
});


angularApp.directive("aquaSliderTimeout", ['$timeout', function ($timeout) {
    return function (scope, elem, attr) {
        $timeout(function () {
            if(scope.$last) {
                Aqua.slider(elem, attr.aquaSlider);
            }
        }, 300);
    }
}]);



angularApp.directive("sliderNoRepeat", function () {
    return function (scope, elem, attr) {
        console.log("directive");
        Aqua.sliderNotRepeat(elem, attr.sliderNoRepeat);
    }
});

angularApp.directive("aquaUnderconstruction", function () {
    return function (scope, elem, attr) {
        elem.countdown({until: '+6o', format: 'odhms'});
    }
});

angularApp.directive("aquaAccordion", function () {
    return function (scope, elem, attr) {

        Aqua.accordion(elem);
    }
});

angularApp.directive("backTop", function () {
    return function (scope, elem, attr) {
        (function ($) {
            $(".back-top").on("click", function () {
                $("html, body").animate({ scrollTop: 0 }, "slow");
            });
            angular.element(document).scroll(function () {
                if( $(this).scrollTop() < 500 ) {
                    $(".back-top").css("display", "none");
                }

                else {
                    $(".back-top").css("display", "block");
                }
            });
        })(jQuery);
    }
});

angularApp.directive("aquaScroll", function () {
    return function (scope, elem, attr) {
        (function ($) {

            $(".nav-title1").children().on("click", function (e) {
                e.preventDefault();
                var $id = $(this).attr('href');
                console.log($($id));
                console.log($($id).length);
                if ($($id).length) {
                    $("html, body").animate({
                        scrollTop: $($id).offset().top
                    }, 1000);
                }
            });

            $(".nav-mobile-menu-hover1").children().on("click", function (e) {
                e.preventDefault();
                var $id = $(this).attr('href');
                console.log($($id));
                console.log($($id).length);
                if ($($id).length) {

                    $("html, body").animate({
                        scrollTop: $($id).offset().top
                    }, 1000);
                }
            });


        })(jQuery);
    }
});

angularApp.directive("pretty", function () {
    return function (scope, elem, attr) {
        $("[rel^='prettyPhoto']").prettyPhoto({deeplinking: false, social_tools: false});
    }
});


angularApp.directive("updateScrollTop", function () {
    return function (scope, elem, attr) {
        console.log("dasdasdasdxc");
        (function ($) {
            $(".down-arrow").on("click", function (e) {
                e.preventDefault();
                $("html, body").animate({
                    scrollTop: $("#update2-ab").offset().top
                }, 1000);
            });
        })(jQuery);
    }
});



angularApp.service('dataService', function () {
    var dataList = [];

    var addData = function (newObj) {
        dataList.push(newObj);
        console.log("Add data");
    };

    var getData = function () {
        return dataList;
    };

    return {
        addData : addData,
        getData : getData
    };
});

